package io.switchbit.repository;

import io.switchbit.model.SdEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SdEntityRepository extends JpaRepository<SdEntity, UUID> {

}
