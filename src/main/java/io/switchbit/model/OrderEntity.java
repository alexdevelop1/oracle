package io.switchbit.model;

import io.switchbit.domain.Order;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "t_order")
public class OrderEntity {

    public static final String NAME = "t_order";
    public static final String SEQUENCE_NAME = NAME + "_seq";

    @Id
    @SequenceGenerator(name = "jpa.Sequence.t.order", sequenceName = "T_ORDER_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "jpa.Sequence.t.order", strategy = GenerationType.SEQUENCE)
    private Long id;

    private String customer;

//    @Type(type = "Order")
    @Type(type = "io.switchbit.persistence.OrderUserType")
    @Column(name = "order_xml", columnDefinition = "XMLType" )
    private Order order;

    public OrderEntity() {
        // for JPA
    }


    public OrderEntity(final Order order) {
        this.customer = order.getCustomer();
        this.order = order;
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getSequenceName() {
        return SEQUENCE_NAME;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public String getCustomer() {
        return customer;
    }

    public Order getOrder() {
        return order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderEntity that = (OrderEntity) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(customer, that.customer) &&
            Objects.equals(order, that.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customer, order);
    }

    @Override
    public String toString() {
        return "OrderEntity{" +
            "id=" + id +
            ", customer='" + customer + '\'' +
            ", order=" + order +
            '}';
    }
}
