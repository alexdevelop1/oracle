package io.switchbit.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;

// import static io.switchbit.persistence.OracleXmlDialect.XMLTYPE;

import java.util.Objects;
import java.util.UUID;

import static io.switchbit.model.SdEntity.NAME;

@Entity(name = NAME)
public class SdEntity {

    static final String NAME = "sd";
    static final String SEQUENCE_NAME = NAME + "_seq";

    @Id
    // @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id = UUID.randomUUID();

//    @Type(type = XMLTYPE)
    @Type(type = "io.switchbit.persistence.XmlAsObjectUserType")
    @Column(name = "doc")
    private Object doc;

    public SdEntity() {
        // for JPA
    }
    public SdEntity(final Object doc) {
        this.doc = doc;
    }

    public UUID getId() {
        return id;
    }

    public Object getDoc() {
        return doc;
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getSequenceName() {
        return SEQUENCE_NAME;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setDoc(Object doc) {
        this.doc = doc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SdEntity sdEntity = (SdEntity) o;
        return Objects.equals(id, sdEntity.id) &&
            Objects.equals(doc, sdEntity.doc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, doc);
    }

    @Override
    public String toString() {
        return "SdEntity{" +
            "id=" + id +
            ", doc=" + doc +
            '}';
    }
}
