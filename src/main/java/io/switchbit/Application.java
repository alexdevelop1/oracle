package io.switchbit;

import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.StringWriter;
import java.util.Locale;

import javax.xml.bind.JAXB;

import io.switchbit.model.OrderEntity;
import io.switchbit.model.SdEntity;
import io.switchbit.repository.OrderEntityRepository;
import io.switchbit.repository.SdEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.switchbit.domain.Order;

@SpringBootApplication
@RestController
public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    private final OrderEntityRepository orderEntityRepository;
    private final SdEntityRepository sdEntityRepository;

    @Autowired
    public Application(OrderEntityRepository orderEntityRepository, SdEntityRepository sdEntityRepository) {
        this.orderEntityRepository = orderEntityRepository;
        this.sdEntityRepository = sdEntityRepository;
    }

    @RequestMapping(value = "/orders", method = POST, consumes = APPLICATION_XML_VALUE)
    public OrderEntity placeOrder(@RequestBody final Order order) {

      //  runMarshalling(order);
        StringWriter orderXml = new StringWriter();
        JAXB.marshal(order, orderXml);
        OrderEntity orderEntity = new OrderEntity(order);
        OrderEntity save = orderEntityRepository.save(orderEntity);


        return save;
    }


    /**
     * получаем
     * @param order
     * @return
     */
    private StringWriter runMarshalling(Order order){

        StringWriter orderXml = new StringWriter();
        JAXB.marshal(order, orderXml);

        return orderXml;
    }


    @RequestMapping(value = "/sds", method = POST, consumes = APPLICATION_XML_VALUE)
    // public SdEntity postSd(@RequestBody final Sd sd) {
    public SdEntity postSd(@RequestBody final String body) {
//        JAXB.marshal(sd, xml);
//        return sdEntityRepository.save(new SdEntity(sd));
        return sdEntityRepository.save(new SdEntity(body));
    }

    public static void main(String[] args) {
        logger.info("Application is starting...");
        System.setProperty("file.encoding","UTF-8");
        Locale.setDefault(new Locale("en","US","UTF-8"));
        // Locale.setDefault(new Locale("ru","RU","UTF-8"));
        SpringApplication.run(Application.class, args);
    }
}
