package io.switchbit.persistence;

import oracle.jdbc.driver.OracleConnection;
import oracle.sql.CLOB;
import oracle.xdb.XMLType;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.*;

import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.xml.bind.Marshaller.JAXB_ENCODING;
// import org.springframework.jdbc.support.nativejdbc.NativeJdbcExtractor;
// import org.springframework.jdbc.support.nativejdbc.OracleJdbc4NativeJdbcExtractor;

public class CustomUserType<T> implements UserType {

    private static final Logger logger = LoggerFactory.getLogger(CustomUserType.class);

    private JAXBContext jaxbContext;
    private Class<T> clazz;

    public CustomUserType(Class<T> clazz) {
        this.clazz = clazz;
        try {
            jaxbContext = JAXBContext.newInstance(clazz);
        } catch (Exception e) {
            throw new RuntimeException("Cannot initialize JAXBContext", e);
        }
    }

    @Override
    public int[] sqlTypes() {
        return new int[]{XMLType._SQL_TYPECODE};
    }

    @Override
    public Class returnedClass() {
        return clazz;
    }

    @Override
    public boolean equals(final Object x, final Object y) {
        return (x != null) && x.equals(y);
    }

    @Override
    public int hashCode(final Object x) {
        return (x != null) ? x.hashCode() : 0;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        XMLType xmlType = (XMLType) rs.getObject(names[0]);
        Object document = null;
        if (xmlType != null) {
            try {
                final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                document = unmarshaller.unmarshal(xmlType.getDocument(), clazz).getValue();
            } catch (JAXBException e) {
                throw new SQLException("Could not unmarshal object", e);
            }
        }

        return document;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        try {
            XMLType xmlType = null;
            if (value != null) {
//                NativeJdbcExtractor extractor = new OracleJdbc4NativeJdbcExtractor();
//                Connection connection = extractor.getNativeConnection(statement.getConnection());
                String xml = "";
                if (value instanceof String) {
                    xml = (String) value;
                } else {
                    xml = jaxbToString(value);
                }
                String xmlWithoutNamespaces = removeNamespacesXSLT(xml);
                //logger.info("XML without spaces: " + xmlWithoutNamespaces);

                Connection connection = st.getConnection();

               // OracleConnection oracleConnection1 = connection.unwrap(OracleConnection.class);

                OracleConnection oracleConnection = getOracleConnection(connection);

                xmlType = new XMLType(oracleConnection, xmlWithoutNamespaces);

            }
            // Important to still set object even if it's null
            // to prevent "org.h2.jdbc.JdbcSQLException: Parameter "#?" is not set; SQL statement"
            st.setObject(index, xmlType);
        } catch (Exception e) {
            throw new SQLException("Could not marshal Order", e);
        }
    }


    private OracleConnection getOracleConnection(Connection conn) throws SQLException {
        CLOB tempClob = null;
        CallableStatement stmt = null;
        try {
            stmt = conn.prepareCall("{ call DBMS_LOB.CREATETEMPORARY(?, TRUE)}");
            stmt.registerOutParameter(1, java.sql.Types.CLOB);
            stmt.execute();
            tempClob = (CLOB) stmt.getObject(1);

            OracleConnection oracleConnection = (OracleConnection) tempClob.getOracleConnection();

            return oracleConnection;

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Throwable e) {
                }
            }
        }
    }


    @Override
    public Object deepCopy(final Object value) {
        return value;
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Serializable disassemble(final Object value) {
        return (Serializable) value;
    }

    @Override
    public Object assemble(final Serializable cached, final Object owner) {
        return cached;
    }

    @Override
    public Object replace(final Object original, final Object target, final Object owner) {
        return original;
    }

    protected String jaxbToString(final Object value) throws JAXBException {
        final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(JAXB_ENCODING, UTF_8.name());

        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(value, stringWriter);

        return stringWriter.toString();
    }

    public static String removeNamespacesRegexp(String xml) {  // issues
        return xml.replaceAll("(<\\?[^<]*\\?>)?", ""). /* remove preamble */
            replaceAll("xmlns.*?(\"|\').*?(\"|\')", "") /* remove xmlns declaration */
            .replaceAll("(<)(\\w+:)(.*?>)", "$1$3") /* remove opening tag prefix */
            .replaceAll("(</)(\\w+:)(.*?>)", "$1$3"); /* remove closing tags prefix */
    }

    public static String removeNamespacesXSLT(String xml) { // good work
        try {
            StringReader reader = new StringReader(xml);
            StringWriter writer = new StringWriter();
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(
                new javax.xml.transform.stream.StreamSource("classpath:no-namespaces.xslt"));  // TODO only for demo purposes, cache transformer into static library to prevent performance issue
            transformer.transform(
                new javax.xml.transform.stream.StreamSource(reader),
                new javax.xml.transform.stream.StreamResult(writer));
            String result = writer.toString();
            return result;
        } catch (Exception e) {
            logger.error("removeNamespacesXSLT: " + e.getMessage());
        }
        return "";
    }
}
