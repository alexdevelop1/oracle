package io.switchbit.persistence;

import io.switchbit.domain.Order;

public class OrderUserType extends CustomUserType<Order> {

    public OrderUserType(Class<Order> clazz) {
        super(clazz);
    }

    public OrderUserType() {
        super(Order.class);
    }


}
