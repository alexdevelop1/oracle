package io.switchbit.persistence;

import io.switchbit.persistence.CustomUserType;

public class XmlAsObjectUserType extends CustomUserType<Object> {

    public XmlAsObjectUserType(Class<Object> clazz) {
        super(clazz);
    }

    public XmlAsObjectUserType() {
        super(Object.class);
    }
}
