package io.switchbit.persistence;

import io.switchbit.domain.Order;
import org.hibernate.boot.model.TypeContributions;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.service.ServiceRegistry;

// import org.hibernate.metamodel.spi.TypeContributions;

// Oleg: unused legacy class, remove it in a real application
public class OracleXmlDialect extends Oracle10gDialect {

    public static final String XMLTYPE = "XMLTYPE";

    public OracleXmlDialect() {
        super();
//        registerColumnType(XMLType._SQL_TYPECODE, XMLTYPE);  // Oleg: commented to avoid side-effects, remove class in case success
//        registerHibernateType(XMLType._SQL_TYPECODE, XMLTYPE);
    }

    @Override
    public void contributeTypes(final TypeContributions typeContributions,
                                final ServiceRegistry serviceRegistry) {
        super.contributeTypes(typeContributions, serviceRegistry);
//        registerTypes(typeContributions);
    }

    private void registerTypes(final TypeContributions typeContributions) {
        typeContributions.contributeType(new CustomUserType<Order>(Order.class), new String[]{"Order"});
        typeContributions.contributeType(new CustomUserType<Object>(Object.class), new String[]{XMLTYPE});
    }
}
